package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        PriorityQueue<Cat> q = new PriorityQueue<Cat>();
        q.add(new Cat(3, "Tom", 5.5, "white"));
        q.add(new Cat(4, "Barsik", 4, "black"));
        q.add(new Cat(2, "Murzik", 10, "red"));

        for(Cat c : q){
            System.out.println(c.getName() + " " + c.getAge() + " " + c.getWeight() + " " + c.getColor());
        }
    }
}