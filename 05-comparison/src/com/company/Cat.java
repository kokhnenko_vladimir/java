package com.company;

public class Cat implements Comparable<Cat> {
    private int Age;
    private String Name;
    private double Weight;
    private String Color;


    public Cat(int age, String name, double weight, String color) {
        Age = age;
        Name = name;
        Weight = weight;
        Color = color;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getWeight() {
        return Weight;
    }

    public void setWeight(double weight) {
        Weight = weight;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    @Override
    public int compareTo(Cat o) {
        int res;

        if(this.Weight < o.Weight) {
            res = -1;
        } else if(this.Weight > o.Weight) {
            res = 1;
        } else {
            res = 0;
        }
        return res;
    }
}
