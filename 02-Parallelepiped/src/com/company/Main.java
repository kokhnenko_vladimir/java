package com.company;

import java.util.Scanner;
import static java.lang.Math.cos;
import static java.lang.Math.sqrt;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Please input length, A: ");
        int A = in.nextInt();
        System.out.print("Please input height, B: ");
        int B = in.nextInt();
        System.out.print("Please input width, C: ");
        int C = in.nextInt();

        System.out.printf("A: %d \n", A);
        System.out.printf("B: %d \n", B);
        System.out.printf("C: %d \n", C);
        in.close();

        int offsetLeft = (int) (C * cos(sqrt(2) / 2));
        int offsetDown = offsetLeft / 2;

        int gHeight = B / 2 + offsetDown;
        int gWidth = A + offsetLeft;

        int stRectW1 = gWidth - A;
        int endRectW1 = gWidth - 1;
        int stRectH1 = 0;
        int endRectH1 = B / 2;

        int stRectW2 = 0;
        int endRectW2 = A;
        int stRectH2 = offsetDown;
        int endRectH2 = gHeight;

        int stTrW1 = offsetLeft - 2;
        int stTrH1 = 1;
        int endTrH1 = offsetDown - 1;

        int stTrW2 = gWidth - 2;
        int stTrH2 = 1;
        int endTrH2 = offsetDown - 1;

        int stTrW3 = offsetLeft - 2;
        int stTrH3 = endRectH1 + 1;
        int endTrH3 = gHeight - 1;

        int stTrW4 = gWidth - 2;
        int stTrH4 = endRectH1 + 1;
        int endTrH4 = gHeight - 1;

        for (int i = 0; i <= gHeight; i++) {
            for (int j = 0; j < gWidth; j++) {
                if ((i == stRectH1 || i == endRectH1) && j >= stRectW1 ||
                        i < endRectH1 && (j == stRectW1 || j == endRectW1) ||
                        (i == stRectH2 || i == endRectH2) && j <= endRectW2 ||
                        i > stRectH2 && i < endRectH2 && (j == stRectW2 || j == endRectW2)) {
                    System.out.print('*');
                } else if (i >= stTrH1 && i <= endTrH1 && j == stTrW1) {
                    System.out.print('*');
                    stTrW1 -= 2;
                } else if(i >= stTrH2 && i <= endTrH2 && j == stTrW2) {
                    System.out.print('*');
                    stTrW2 -= 2;
                } else if(i >= stTrH3 && i <= endTrH3 && j == stTrW3) {
                    System.out.print('*');
                    stTrW3 -= 2;
                } else if(i >= stTrH4 && i <= endTrH4 && j == stTrW4) {
                    System.out.print('*');
                    stTrW4 -= 2;
                } else {
                    System.out.print(' ');
                }
            }
            System.out.println();
        }
    }
}